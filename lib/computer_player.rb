class ComputerPlayer
  LETTERS = ('a'..'z').to_a

  attr_reader :dictionary, :secret_length, :secret_word, :guessed_letters

  def self.read_dictionary(filename)
    File .readlines(filename).map(&:chomp)
  end

  def initialize(dictionary)
    @dictionary = dictionary
    @secret_word = dictionary.sample
  end

  def pick_secret_word
    secret_word.length
  end

  def check_guess(letter)
    raise_error "Guess is not a letter" unless LETTERS.include?(letter)

    found_indices = []

    (0...pick_secret_word).each do |i|
      found_indices << i if secret_word[i] == letter
    end

    found_indices
  end

  def register_secret_length(length)
    @secret_length = length
  end

  def guess(board)
    LETTERS.sample
  end

  def handle_response(letter, found_indices)
    candidate_words.select! do |word|
      found_indices.all?{|i| word[i] == letter} && (
      word.count(letter) == found_indices.size)
    end
  end

  def candidate_words
    dictionary
  end

  def register_secret_length(length)
    candidate_words.select!{|word| word.length == length}
  end

  def guess(board)
    letter_counts = letter_freq
    filtered_letter_count = letter_counts.reject{|k, v| board.include?(k)}
    most_common(filtered_letter_count)
  end

  private

  def letter_freq
    letter_freq = Hash.new(0)
    candidate_letters = candidate_words.join.chars

    candidate_letters.uniq.each do |char|
      letter_freq[char] = candidate_letters.count(char)
    end

    letter_freq
  end

  def most_common(letter_counts)
    letter_counts.sort_by{|letter, counts| counts}.last.first
  end
end
