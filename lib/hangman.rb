require_relative 'human_player'
require_relative 'computer_player'

class Hangman
  MAX_GUESSES = 7
  attr_reader :guesser, :referee, :board, :guessed_letters

  def initialize(players)
    @guesser = players[:guesser]
    @referee = players[:referee]
    @board, @guessed_letters = [], []
    @lives = MAX_GUESSES
  end

  def setup
    secret_length = @referee.pick_secret_word
    @guesser.register_secret_length(secret_length)
    @board = Array.new(secret_length)
  end

  def take_turn
    display_board
    guess = @guesser.guess(board)
    indices = @referee.check_guess(guess)
    @lives -= 1 if indices.empty?
    update_board(indices, guess)
    @guesser.handle_response(guess, indices)
  end

  def play
    setup
    until over? do
      take_turn
    end

    winner
  end

  private

  def update_board(found_letters, guess)
    guessed_letters << guess
    found_letters.each do |i|
      board[i] = guess
    end
  end

  def display_board
    board_string = board.map{|el| el ? el : "_"}.join
    puts "You have #{@lives} guesses left."
    print "letters guessed #{guessed_letters.uniq.sort}\n"
    puts "Secret word: #{board_string}"
  end

  def over?
    board.count(nil) == 0 || @lives == 0
  end

  def winner
    if @lives == 0
      puts "You lose, the secret word was #{referee.secret_word}"
    else
      puts "You win! The secret word was #{referee.secret_word}"
    end
  end
end

if __FILE__ == $PROGRAM_NAME
  filename = "dictionary.txt"
  dictionary = ComputerPlayer.read_dictionary(filename)

  print "Guessor: Computer (Y/N)? "
  if gets.upcase.chomp == "Y"
    guesser = ComputerPlayer.new(dictionary)
  else
    guesser = HumanPlayer.new
  end

  print "Referee: Computer (Y/N)? "
  if gets.upcase.chomp == "Y"
    referee = ComputerPlayer.new(dictionary)
  else
    referee = HumanPlayer.new
  end

  players = {guesser: guesser, referee: referee}

  Hangman.new(players).play
end
