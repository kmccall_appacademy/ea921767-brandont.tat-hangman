class HumanPlayer
  def initialize
  end

  def register_secret_length(length)
    puts "Secret word is #{length} letters long"
  end

  def pick_secret_word
    print "Think of a secret word and enter its length: "
    gets.chomp.to_i
  end

  def guess(board)
    print "> "
    guess = gets.downcase.chomp
    if board.guessed_letters.include?(guess)
      puts "You already guessed '#{guess}', please try again."
      guess(board)
    end
    guessed_letters << guess
    guess
  end

  def check_guess(letter)
    puts "Guesser guessed the letter '#{letter}'"
    print "What indices does this occur at? Please enter in form x,x,x: "
    gets.chomp.split(',').map(&:to_i)
  end

  def handle_response(guess, indices)
    if indices.empty?
      puts "Secret word does not have the letter #{guess}"
    else
      puts "Found #{guess} at #{indices}"
    end
  end

  def secret_word
    print "What was the secret word? "
    gets.chomp
  end
end
